package com.example.demo.view

import com.example.demo.app.Styles
import com.example.demo.controller.MainController
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.control.ScrollPane
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import sun.plugin.javascript.navig.Anchor
import tornadofx.*
import java.awt.Color
import javax.swing.plaf.metal.MetalIconFactory

class MainView : View("RainbowLog") {
    private val mainController: MainController by inject()

    override val root = anchorpane {
        var regionUrl: TextField by singleAssign()
        var resultId: TextField by singleAssign()
        var settings: VBox by singleAssign()
        var devlog: ScrollPane by singleAssign()

        devlog = scrollpane {
            hbarPolicy = ScrollPane.ScrollBarPolicy.NEVER
            vbarPolicy = ScrollPane.ScrollBarPolicy.AS_NEEDED
            content = label {
                bind(mainController.labelText)
                addClass(Styles.log)
            }
        }
        settings = vbox {
            squeezebox {
                fold("Messages", expanded = true) {
                    squeezebox {
                        vbox {
                            alignment = Pos.TOP_LEFT
                            spacing = 10.0
                            fold("Incoming", expanded = true) {
                                checkbox("RawTextChannelMessage", mainController.iRawBool)
                                checkbox("RecognizedSpeechMessage", mainController.iRecognizedBool)
                                checkbox("OpenSessionChannelMessage", mainController.iOpenBool)
                                checkbox("OpeningSessionChannelMessage", mainController.iOpeningBool)
                                checkbox("OpenedSessionChannelMessage", mainController.iOpenedBool)
                                checkbox("ClosedSessionChannelMessage", mainController.iClosedBool)
                                checkbox("SpeechChannelMessage", mainController.iSpeechBool)
                            }
                            fold("Outgoing", expanded = true) {
                                checkbox("RawTextChannelMessage", mainController.oRawBool)
                                checkbox("OpenSessionChannelMessage", mainController.oOpenBool)
                                checkbox("CloseSessionChannelMessage", mainController.oCloseBool)
                            }
                            fold("General", expanded = true) {
                                checkbox("NodeSwitchNotificationMessage", mainController.gNodeSwitchBool)
                                checkbox ( "PreparationBatchRequestMessage", mainController.gPreparationBatchBool)
                                checkbox("phraseId", mainController.gPhraseIdBool)
                            }
                        }
                    }
                }
            }
            form {
                fieldset("Parameters") {
                    field("Region URL") {
                        regionUrl = textfield("")
                        tooltip("region of resultid call (ex. https://api.us.dasha.ai)")
                    }
                    field("ResultId") {
                        resultId = textfield("")
                        tooltip( "resultid (not conversationid!)")
                    }
                }
                button("Show DevLog") {
                    tooltip("shows devlog based on checked messages")
                    action {
                        mainController.labelText.value = ""
                        mainController.showDevLog(regionUrl.text, resultId.text)
                    }
                }
            }
        }
        AnchorPane.setTopAnchor(settings, 10.0)
        AnchorPane.setRightAnchor(settings, 10.0)
        AnchorPane.setTopAnchor(devlog, 10.0)
        AnchorPane.setBottomAnchor(devlog, 10.0)
        AnchorPane.setLeftAnchor(devlog, 10.0)
        AnchorPane.setRightAnchor(devlog, 300.0)
    }
}