package com.example.demo.app

import javafx.scene.text.FontWeight
import tornadofx.*
import javafx.scene.paint.Color

class Styles : Stylesheet() {
    companion object {
        val heading by cssclass()
        val log by cssclass()
    }

    init {
        label and heading {
            padding = box(10.px)
            fontSize = 20.px
            fontWeight = FontWeight.BOLD
        }
        log {
            fontFamily = "Courier New"
        }
    }
}