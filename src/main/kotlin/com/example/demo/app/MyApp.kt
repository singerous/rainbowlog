package com.example.demo.app

import com.example.demo.view.MainView
import javafx.stage.Screen
import javafx.stage.Stage
import tornadofx.App

class MyApp: App(MainView::class, Styles::class) {
    override fun start(stage: Stage) {
        with(stage) {
            width = Screen.getPrimary().bounds.width
            height = Screen.getPrimary().bounds.height
            if (width <= 800 && height <= 600) {
                width = 700.0
                height = 500.0
            } else if (width <= 1280 && height <= 768) {
                width = 1000.0
                height = 700.0
            } else if (width <= 1920 && height <= 1080) {
                width = 1200.0
                height = 800.0
            }
            minWidth = width
            minHeight = height
        }
        super.start(stage)
    }
}