package com.example.demo.controller

data class Message(
    val msgId: String,
    val from: String?,
    val to: String?,
    val msg: RawMessage?,
    val transition: Any?,
    val status: String?,
    val text: String?,
    val phraseId: String?,
    val textMsgId: String?,
    val voiceSegmentId: String?,
    val type: String?,
    val originalText: String?
)