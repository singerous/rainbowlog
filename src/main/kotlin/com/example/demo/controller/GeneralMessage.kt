package com.example.demo.controller

import java.util.*

data class GeneralMessage(
    val time: Date,
    val msg: Message,
    val incoming: Boolean
)
