package com.example.demo.controller

import com.google.gson.Gson
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.Controller
import okhttp3.*
import sun.security.ec.point.ProjectivePoint
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*



class MainController: Controller() {
    private val dashaAPIToken: String = System.getenv("DASHA_APIKEY") ?: ""
    private val client = OkHttpClient()
    private lateinit var msgs: Array<GeneralMessage>

    private var prettyLog: MutableList<String> = MutableList<String>(0) {"it = $it"}
    private var prettyTimes: MutableList<String> = MutableList<String>(0) {"it = $it"}
    private var prettyNature: MutableList<String> = MutableList<String>(0) {"it = $it"}
    private var prettyMsgs: MutableList<GeneralMessage> = MutableList<GeneralMessage>(0) {
        GeneralMessage(Date(), Message(
                "","","", RawMessage(
                "","","",""
        ),"","","","","","","",""
        ),false)
    }

    private var openedSessionChannelIndex: Int = 0
    private var openedSessionChannelMsgIndex: Int = 0

    public var labelText: SimpleStringProperty = SimpleStringProperty()
    public var iRawBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var iRecognizedBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var iOpenBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var iOpeningBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var iOpenedBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var iClosedBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var iSpeechBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var oRawBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var oOpenBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var oCloseBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var gNodeSwitchBool: SimpleBooleanProperty = SimpleBooleanProperty(true)
    public var gPreparationBatchBool: SimpleBooleanProperty = SimpleBooleanProperty(false)
    public var gPhraseIdBool: SimpleBooleanProperty = SimpleBooleanProperty(false)

    fun showDevLog(regionUrl: String, resultId: String) {
        openedSessionChannelIndex = 0
        openedSessionChannelMsgIndex = 0
        prettyLog.clear()
        prettyMsgs.clear()
        prettyNature.clear()
        prettyTimes.clear()
        try {
            if (initDevLog(regionUrl, resultId)) {
                formPrettyLog()
            }
        } catch (e: java.lang.IllegalArgumentException) {
            labelText.value = "wrong arguments"
        }
    }

    private fun initDevLog(regionUrl: String, resultId: String): Boolean {
        val request = Request.Builder()
                .url("$regionUrl/api/v1/conversations/results/$resultId/logsDev")
                .header("Authorization", "Bearer $dashaAPIToken")
                .build()
        lateinit var response: Response
        try {
            response = client.newCall(request).execute()
        } catch (e: java.net.UnknownHostException) {
            labelText.value = "UnknownHostException. check if region url correct"
        }
        if (response.isSuccessful) {
            msgs = Gson().fromJson(response.body!!.string(), Array<GeneralMessage>::class.java)
            return true
        } else {
            msgs = arrayOf()
            labelText.value = ("Unexpected code ${response.code}")
            return false
        }
    }

    private fun formPrettyLog() {
        for ((i, message) in msgs.withIndex()) {
            if (message.msg.msgId == "OpenedSessionChannelMessage") {
                openedSessionChannelMsgIndex = i
            }
            processMessage(message)
        }
        for ((i, message) in prettyLog.withIndex()) {
            //holy crutch (3:26am good time for bad ideas)
            if (message == "[OpenedSessionChannelMessage]   |") {
                openedSessionChannelIndex = i
            }
        }
        var log  = ""
        //3:54am this is so damn bad but aof
        for ((i, message) in prettyMsgs.withIndex()) {
            when(message.msg.msgId) {
                "OpenSessionChannelMessage" -> {
                    if (message.incoming and iOpenBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                    if (!message.incoming and oOpenBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "NodeSwitchNotificationMessage" -> {
                    if (gNodeSwitchBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "PreparationBatchRequestMessage" -> {
                    if (gPreparationBatchBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "OpeningSessionChannelMessage" -> {
                    if (iOpeningBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "OpenedSessionChannelMessage" -> {
                    if (iOpenedBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "SpeechChannelMessage" -> {
                    if (message.incoming and iSpeechBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "RawTextChannelMessage" -> {
                    if (message.incoming and iRawBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                    if (!message.incoming and oRawBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "RecognizedSpeechMessage" -> {
                    if (iRecognizedBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "CloseSessionChannelMessage" -> {
                    if (message.incoming and iClosedBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                    if (!message.incoming and oCloseBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
                "ClosedSessionChannelMessage" -> {
                    if (iClosedBool.value) {
                        log += prettyTimes[i] + prettyNature[i] + prettyLog[i] + "\n"
                    }
                }
            }
        }
        labelText.value = log
    }

    private fun processMessage(message: GeneralMessage) {
        when(message.msg.msgId) {
            "OpenSessionChannelMessage" -> prettyProcess(message, ::formOpenSessionMessage)
            "NodeSwitchNotificationMessage" -> prettyProcess(message, ::formNodeSwitchMessage)
            "PreparationBatchRequestMessage" -> prettyProcess(message, ::formPreparationBatchMessage)
            "OpeningSessionChannelMessage" -> prettyProcess(message, ::formOpeningSessionMessage)
            "OpenedSessionChannelMessage" -> prettyProcess(message, ::formOpenedSessionMessage)
            "SpeechChannelMessage" -> prettyProcess(message, ::formSpeechChannelMessage)
            "RawTextChannelMessage" -> prettyProcess(message, ::formRawTextChannelMessage)
            "RecognizedSpeechMessage" -> prettyProcess(message, ::formRecognizedSpeechMessage)
            "CloseSessionChannelMessage" -> prettyProcess(message, ::formCloseSessionMessage)
            "ClosedSessionChannelMessage" -> prettyProcess(message, ::formClosedSessionMessage)
        }
    }

    private fun formClosedSessionMessage(message: GeneralMessage): String {
        val prettyMessage = "[ClosedSessionChannelMessage]   |"
        return prettyMessage
    }

    private fun formCloseSessionMessage(message: GeneralMessage): String {
        val prettyMessage = "[CloseSessionChannelMessage]    |"
        return prettyMessage
    }

    private fun formRecognizedSpeechMessage(message: GeneralMessage): String {
        val prettyMessage = "[RecognizedSpeechMessage]       | (${message.msg.voiceSegmentId}) by text: ${message.msg.originalText}"
        return prettyMessage
    }

    private fun formRawTextChannelMessage(message: GeneralMessage) : String {
        var prettyMessage = "[RawTextChannelMessage]         |"
        if (message.msg.type != "PrefetchedLegacy") {
            prettyMessage += " (${message.msg.voiceSegmentId}) {${message.msg.textMsgId}}"
        }
        prettyMessage += " by text: ${message.msg.text}"
        if ((message.msg.type == "PrefetchedLegacy") and gPhraseIdBool.value) {
            prettyMessage += " (phraseId: ${message.msg.phraseId})"
        }
        return prettyMessage
    }

    private fun formSpeechChannelMessage(message: GeneralMessage) : String {
        val prettyMessage = "[SpeechChannelMessage]          | (${message.msg.voiceSegmentId}) ${message.msg.type}"
        return prettyMessage
    }

    private fun formOpenedSessionMessage(message: GeneralMessage) : String {
        val prettyMessage = "[OpenedSessionChannelMessage]   |"
        return prettyMessage
    }

    private fun formOpeningSessionMessage(message: GeneralMessage) : String {
        val prettyMessage = "[OpeningSessionChannelMessage]  |"
        return prettyMessage
    }

    private fun prettyProcess(message: GeneralMessage, formMessage: (type: GeneralMessage) -> String) {
        prettyLog.add(formMessage(message))
        prettyTimes.add(formPrettyTime(message))
        prettyNature.add(formPrettyNature(message))
        prettyMsgs.add(message)
    }

    private fun formPrettyTime(message: GeneralMessage): String {
        val rootTime = msgs[openedSessionChannelMsgIndex].time
        val deltaTime = message.time.time - rootTime.time
        val time = Date(deltaTime)
        val format = SimpleDateFormat("mm:ss.SSS")
        if ((openedSessionChannelMsgIndex == 0) and (message.msg.msgId != "OpenedSessionChannelMessage")) {
            return "         "
        } else {
            return format.format(time)
        }
    }

    private fun formPreparationBatchMessage(message: GeneralMessage): String {
        val prettyMessage = "[PreparationBatchRequestMessage]| (preparing phrases)"
        return prettyMessage
    }

    private fun formPrettyNature(message: GeneralMessage): String {
        if (message.incoming) {
            return " | i | "
        } else {
            return " | o | "
        }
    }

    private fun formNodeSwitchMessage(message: GeneralMessage): String {
        var prettyMessage = "${message.msg.from} -> ${message.msg.to} with ${message.msg.msgId}"
        if (message.msg.msgId == "RecognizedSpeechMessage") {
            prettyMessage += " by text: ${message.msg.msg?.originalText}"
        }
        return prettyMessage
    }

    private fun formOpenSessionMessage(message: GeneralMessage): String {
        val prettyMessage = "[OpenSessionChannelMessage]     |"
        return prettyMessage
    }
}