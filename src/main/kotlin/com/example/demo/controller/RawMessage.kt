package com.example.demo.controller

data class RawMessage(
        val msgId: String,
        val type: String?,
        val voiceSegmentId: String?,
        val originalText: String?
)